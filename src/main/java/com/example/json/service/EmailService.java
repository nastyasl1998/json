package com.example.json.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    private ClientAPI clientAPI;

    public Boolean emailCheck(String email) {
        return clientAPI.emailCheck(email);
    }
}
