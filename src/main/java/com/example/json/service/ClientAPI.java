package com.example.json.service;

import com.googlecode.jsonrpc4j.JsonRpcParam;

public interface ClientAPI {
    Boolean emailCheck(@JsonRpcParam(value = "email") String email);
}
