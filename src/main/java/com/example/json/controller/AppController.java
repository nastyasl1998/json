package com.example.json.controller;

import com.example.json.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


@RestController
public class AppController {

    @Value("${parallelExectuion}")
    private Boolean parallelExecutionEnabled;

    static final Logger LOGGER = LoggerFactory.getLogger(AppController.class);

    @Autowired
    private EmailService emailService;

    @RequestMapping("/checkEmail")
    public Boolean checkEmail(@RequestParam(value = "email") String email) {
        boolean check = emailService.emailCheck(email);
        LOGGER.info("Email:" + email);
        return check;
    }

    @RequestMapping("/getNumberOfCharacters")
    public List<Integer> getNumberOfCharacters(@RequestParam(value = "firstUrl") String firstUrl,
                                               @RequestParam(value = "secondUrl") String secondUrl)
            throws InterruptedException {
        List<Integer> list = new CopyOnWriteArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        if (parallelExecutionEnabled) {
            Thread thread = new Thread(() -> {
                ResponseEntity<String> responseEntity = restTemplate.getForEntity(firstUrl, String.class);
                list.add(responseEntity.getBody().length());
            });
            thread.start();
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(secondUrl, String.class);
            int length = responseEntity.getBody().length();
            thread.join();
            list1.add(list.get(0));
            list1.add(length);
        } else {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(firstUrl, String.class);
            list1.add(responseEntity.getBody().length());
            ResponseEntity<String> responseEntity1 = restTemplate.getForEntity(secondUrl, String.class);
            list1.add(responseEntity1.getBody().length());
        }
        return list1;
    }
}
