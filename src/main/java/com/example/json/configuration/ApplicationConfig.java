package com.example.json.configuration;


import com.example.json.service.ClientAPI;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ApplicationConfig {
    private static final String ENDPOINT = "http://localhost:8081/check";

    @Bean
    public JsonRpcHttpClient jsonRpcHttpClient() {
        URL url = null;
        Map<String, String> map = new HashMap<>();
        try {
            url = new URL(ApplicationConfig.ENDPOINT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new JsonRpcHttpClient(url, map);
    }

    @Bean
    public ClientAPI exampleClientAPI(JsonRpcHttpClient jsonRpcHttpClient) {
        return ProxyUtil.createClientProxy(getClass().getClassLoader(), ClientAPI.class, jsonRpcHttpClient);
    }
}
